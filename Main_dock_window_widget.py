#! QT libraries
from PyQt6 import QtWidgets, QtCore


class Main_dock_window_widget(QtWidgets.QMainWindow):
    """Class that serves as a docking window for other widgets

    Args:
        QtWidgets (_type_): inherits from QMainWindow
    """    
    def __init__(self, doackable_widgets: list):
        """Class constructor

        Args:
            doackable_widgets (list): list of all widgets that you want docked. Each widget needs to have a property 'widget_name'(used for naming the tabs)
        """        
        super().__init__()

        self.doackable_widgets = doackable_widgets

        #* Count the widget that will be docked
        self.dock_widget_count = len(self.doackable_widgets)

        #* Find the widget with largest geometry and resize the MainWindow to fit that widget
        max_width = 0
        max_height = 0
        for idx in range(self.dock_widget_count):
            _geom = doackable_widgets[idx].geometry()

            if _geom.width() > max_width:
                max_width = _geom.width()

            if _geom.height() > max_height:
                max_height = _geom.height()

        margin = 40
        self.resize(max_width + margin, max_height + margin)
        

        #* Put all the widgets in the docking area
        self.dock_widgets = [None]*self.dock_widget_count # Make a bunch of dock widgets in the main window
        
        for idx in range(self.dock_widget_count):
            self.dock_widgets[idx] = QtWidgets.QDockWidget(doackable_widgets[idx].widget_name, self)
            self.dock_widgets[idx].setWidget(doackable_widgets[idx])
            self.dock_widgets[idx].setFloating(False)
            self.addDockWidget(QtCore.Qt.DockWidgetArea.TopDockWidgetArea, self.dock_widgets[idx])

        #* Tabify the widgets
        for idx in range(self.dock_widget_count - 1):
            self.tabifyDockWidget(self.dock_widgets[idx], self.dock_widgets[idx + 1])

        #* Activate widget of given index
        active_idx = 0 # 0 by default, choose index based on what widget you want active upon launch
        self.dock_widgets[active_idx].raise_()

    def closeEvent(self, event):
        """Sequence of commands to run before the main dock window closes
        """        
        
        #* Close all widgets; this calls closeEvent() in other widgets which should properly terminate all the hardware
        for idx in range(self.dock_widget_count):
            self.doackable_widgets[idx].close()

        event.accept()         



if __name__ == '__main__':

    #! Miscellaneous
    import sys

    #! Widgets that I want docked
    import Xenics_Camera_widget as Xenics_Camera_widget_class
    import Thorlabs_PM100USB_widget as Thorlabs_PM100USB_widget_class

    app = QtWidgets.QApplication(sys.argv)


    # Camera widget
    Xenics_Camera_w = Xenics_Camera_widget_class.Xenics_camera_widget(skip_enumerate = False, cam_url = "cam://0")

    # Thorlabs powermeter
    Thorlabs_PM100USB_w = Thorlabs_PM100USB_widget_class.Thorlabs_PM100USB_widget()
    

    # All widgets to display as docks
    dockable_widgets = [Xenics_Camera_w, Thorlabs_PM100USB_w]

    # Display Main dock window
    window = Main_dock_window_widget(dockable_widgets)
    window.show()

    sys.exit(app.exec())