# ModeLab share

Miscellaneous code that might be useful for others.

## Installation
This will be updated as the repository gets populated.

## Support
In case of any encountered issues (very likely at this stage), let me know via issue tracker or at my email: m.ploschner@uq.edu.au

## Roadmap
The code is in a pretty raw state. It contains a lot of debugging flags and I made no attempts to optimise, beautify or streamline it. I am happy to make the code much more accessible if there is interest. 

## Contributing
I am happy to implement contributions and suggestions from others and make the code of higher-quality.

## Authors and acknowledgment
Author: Martin Ploschner. 
Acknowledgement: Thanks to all those amazing people on https://stackoverflow.com/ finding solutions to all my questions. 

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

If you use the code in your research endeavours, please cite the following work https://arxiv.org/abs/2202.01932

## Project status
Active
