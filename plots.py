import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

from mathutils import cart2pol

from ipywidgets import interactive, fixed
import ipywidgets as widgets

import os
import time

# Load custom modules
from fields import Field

class Figure:
   def __init__(self, nrows = 1, ncols = 1, width = 5, height = 4, wspace = 0.2, hspace = 0.2):    
      self.fig, self.axes = plt.subplots(nrows = nrows, ncols = ncols)
      self.fig.set_size_inches(width, height)
      self.fig.subplots_adjust(wspace = wspace, hspace = hspace)

      if nrows == 1 and ncols == 1:
         self.figureLayout = 'Single'
      elif nrows == 1 or ncols == 1:
         self.figureLayout = '1D'
      else:    
         self.figureLayout = '2D'

   def plot1D(self, x, y, line_style = '-', color = [1,0,0], pos = (0,0), **kwargs):

      if self.figureLayout == 'Single':
         self.plot_axis = self.axes
      elif self.figureLayout == '1D':
         self.plot_axis = self.axes[pos[0] + pos[1]]
      elif self.figureLayout == '2D': 
         self.plot_axis = self.axes[pos[0], pos[1]]

      title = kwargs.pop('title','')
      xlabel = kwargs.pop('xlabel','')
      ylabel = kwargs.pop('ylabel','')

      if kwargs:
         raise TypeError("plot1D() got an unexpected keyword argument '%s'"
                        % (list(kwargs)[0],))

      self.plot_axis.set_title(title)
      self.plot_axis.set(xlabel = xlabel, ylabel = ylabel)
      self.plot_axis.plot(x,y,line_style, color = color)


   def loglog1D(self, x, y, line_style = '-', color = [1,0,0], pos = (0,0), **kwargs):

      if self.figureLayout == 'Single':
         self.plot_axis = self.axes
      elif self.figureLayout == '1D':
         self.plot_axis = self.axes[pos[0] + pos[1]]
      elif self.figureLayout == '2D': 
         self.plot_axis = self.axes[pos[0], pos[1]]

      title = kwargs.pop('title','')
      xlabel = kwargs.pop('xlabel','')
      ylabel = kwargs.pop('ylabel','')

      if kwargs:
         raise TypeError("plot1D() got an unexpected keyword argument '%s'"
                        % (list(kwargs)[0],))

      self.plot_axis.set_title(title)
      self.plot_axis.set(xlabel = xlabel, ylabel = ylabel)
      self.plot_axis.loglog(x,y,line_style, color = color)           


   def plot(self, Field, plotType = 'E', colormap = 'viridis', pos = (0,0), 
            line_style = '-', color = [1,0,0], **kwargs):

      space = kwargs.pop('space','normal')   
      title = kwargs.pop('title','')

      if self.figureLayout == 'Single':
         pltAx = self.axes
      elif self.figureLayout == '1D':
         pltAx = self.axes[pos[0] + pos[1]]
      elif self.figureLayout == '2D': 
         pltAx = self.axes[pos[0], pos[1]]

      if not isinstance(Field,list): # Field input can be a list but then it plots modes    

         if Field.option == 'x':  
            pltAx.set_title(title)
            pltAx.set(xlabel = r'$x\,[\mu m]$', ylabel = r'arb')

            pltAx.set_xticks([Field.minX, 0, Field.maxX])
            xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
            pltAx.set_xticklabels(xticklabels, color='black')  
         
            if plotType == 'normE':
               ymax = np.abs(Field.E).max()
               pltAx.set_yticks([0, ymax])  
               yticklabels = [0, f'{ymax:.2E}']   
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0, 1.1*ymax)

               pltAx.plot(Field.X, np.abs(Field.E), line_style, color = color)
            elif plotType == 'I':
               ymax = (np.abs(Field.E)**2).max()
               pltAx.set_yticks([0, ymax])
               yticklabels = [0, f'{ymax:.2E}']
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0,1.1*ymax)

               pltAx.plot(Field.X, np.abs(Field.E)**2, line_style, color = color)
               
         elif Field.option == 'y':
            pltAx.set_title(title)
            pltAx.set(xlabel = r'$y\,[\mu m]$', ylabel = r'arb')

            pltAx.set_xticks([Field.minY, 0, Field.maxY])
            xticklabels = [f'{Field.minY*1e6:.2f}', 0, f'{Field.maxY*1e6:.2f}']
            pltAx.set_xticklabels(xticklabels, color='black')  
         
            if plotType == 'normE':
               ymax = np.abs(Field.E).max()
               pltAx.set_yticks([0, ymax])  
               yticklabels = [0, f'{ymax:.2E}']   
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0, 1.1*ymax)

               pltAx.plot(Field.ylabel, np.abs(Field.E), line_style, color = color)
            elif plotType == 'I':
               ymax = (np.abs(Field.E)**2).max()
               pltAx.set_yticks([0, ymax])
               yticklabels = [0, f'{ymax:.2E}']
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0,1.1*ymax)

               pltAx.plot(Field.Y, np.abs(Field.E)**2, line_style, color = color)     
            
         elif Field.option == 'z':  
            pltAx.set_title(title)
            pltAx.set(xlabel = r'$z\,[\mu m]$', ylabel = r'arb')

            pltAx.set_xticks([Field.minZ, 0, Field.maxZ])
            xticklabels = [f'{Field.minZ*1e6:.2f}', 0, f'{Field.maxZ*1e6:.2f}']
            pltAx.set_xticklabels(xticklabels, color='black')  
         
            if plotType == 'normE':
               ymax = np.abs(Field.E).max()
               pltAx.set_yticks([0, ymax])  
               yticklabels = [0, f'{ymax:.2E}']   
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0, 1.1*ymax)

               pltAx.plot(Field.Z, np.abs(Field.E), line_style, color = color)
            elif plotType == 'I':
               ymax = (np.abs(Field.E)**2).max()
               pltAx.set_yticks([0, ymax])
               yticklabels = [0, f'{ymax:.2E}']
               pltAx.set_yticklabels(yticklabels, color='black')
               pltAx.set_ylim(0,1.1*ymax)

               pltAx.plot(Field.Z, np.abs(Field.E)**2, line_style, color = color)

         elif Field.option == 'xy': 
            xlabel = r'$x\,[\mu m]$'
            ylabel = r'$y\,[\mu m]$'
            xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
            yticklabels = [f'{Field.maxY*1e6:.2f}', 0, f'{Field.minY*1e6:.2f}']
            
            if plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field.E))
            elif plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field.E)**2))
               else:
                  pltAx.imshow(np.abs(Field.E)**2, cmap = colormap)

            self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                    xticklabels, yticklabels, Field.dimX, Field.dimY)         

         elif Field.option == 'yz':    
            xlabel = r'$y\,[\mu m]$'
            ylabel = r'$z\,[\mu m]$'
            xticklabels = [f'{Field.minY*1e6:.2f}', 0, f'{Field.maxY*1e6:.2f}']
            yticklabels = [f'{Field.maxZ*1e6:.2f}', 0, f'{Field.minZ*1e6:.2f}']
            
            if plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field.E))
            elif plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field.E)**2))
               else:
                  pltAx.imshow(np.abs(Field.E)**2, cmap = colormap)

            self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                    xticklabels, yticklabels, Field.dimY, Field.dimZ)
         elif Field.option == 'xz':    
            xlabel = r'$x\,[\mu m]$'
            ylabel = r'$z\,[\mu m]$'
            xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
            yticklabels = [f'{Field.maxZ*1e6:.2f}', 0, f'{Field.minZ*1e6:.2f}']
            
            if plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field.E))
            elif plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field.E)**2))
               else:
                  pltAx.imshow(np.abs(Field.E)**2, cmap = colormap)

            self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                    xticklabels, yticklabels, Field.dimX, Field.dimZ)                              

         elif Field.option == 'xyk':
            if space == 'normal':
               xlabel = r'$x\,[\mu m]$'
               ylabel = r'$y\,[\mu m]$'
               xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
               yticklabels = [f'{Field.maxY*1e6:.2f}', 0, f'{Field.minY*1e6:.2f}']
            elif space == 'fourier':
               xlabel = r'$k_x\,[m^{-1}]$'
               ylabel = r'$k_y\,[m^{-1}]$'
               xticklabels = [f'{Field.kminX:.2E}', 0, f'{Field.kmaxX:.2E}']
               yticklabels = [f'{Field.kmaxY:.2E}', 0, f'{Field.kminY:.2E}']

            if space == 'normal' and plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field.E))
            elif space == 'normal' and plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field.E)**2))
               else:
                  pltAx.imshow(np.abs(Field.E)**2, cmap = colormap)
            elif space == 'fourier' and plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field.Sk))
            elif space == 'fourier' and plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field.Sk)**2))
               else:   
                  pltAx.imshow(np.abs(Field.Sk)**2, cmap = colormap)

            self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                 xticklabels, yticklabels, Field.dimX, Field.dimY)            
               
         # deals with plotting 2D (xy) swept (nu) fields that can propagate (k) 
         elif Field.option == 'xyknu':
            if space == 'normal':
               xlabel = r'$x\,[\mu m]$'
               ylabel = r'$y\,[\mu m]$'
               xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
               yticklabels = [f'{Field.maxY*1e6:.2f}', 0, f'{Field.minY*1e6:.2f}']
            elif space == 'fourier':
               xlabel = r'$k_x\,[m^{-1}]$'
               ylabel = r'$k_y\,[m^{-1}]$'
               xticklabels = [f'{Field.kminX:.2E}', 0, f'{Field.kmaxX:.2E}']
               yticklabels = [f'{Field.kmaxY:.2E}', 0, f'{Field.kminY:.2E}']

            required_kwargs = ['nuIdx']
            if all (key in kwargs for key in required_kwargs):
               nuIdx = kwargs.get('nuIdx')
               title = f'{1e9/Field.samplingNU[nuIdx]:0.2f} nm'

               if space == 'normal' and plotType == 'E':
                  pltAx.imshow(ComplexArrayToRgb(Field.E[:,:,nuIdx]))
               elif space == 'normal' and plotType == 'I':
                  if colormap == 'complex':
                     pltAx.imshow(ComplexArrayToRgb(np.abs(Field.E[:,:,nuIdx])**2))
                  else:
                     pltAx.imshow(np.abs(Field.E[:,:,nuIdx])**2, cmap = colormap)
               elif space == 'fourier' and plotType == 'E':
                  pltAx.imshow(ComplexArrayToRgb(Field.Sk[:,:,nuIdx]))
               elif space == 'fourier' and plotType == 'I':
                  if colormap == 'complex':
                     pltAx.imshow(ComplexArrayToRgb(np.abs(Field.Sk[:,:,nuIdx])**2))
                  else:   
                     pltAx.imshow(np.abs(Field.Sk[:,:,nuIdx])**2, cmap = colormap)

               self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                          xticklabels, yticklabels, Field.dimX, Field.dimY)          
            else:
               raise Exception('plot() requires "nuIdx" keyword argument')

         
      # deals with plotting fibre modes (swept case)
      elif isinstance(Field,list):
         required_kwargs = ['nuIdx', 'modeIdx']
         if all (key in kwargs for key in required_kwargs):
            nuIdx = kwargs.get('nuIdx')
            modeIdx = kwargs.get('modeIdx')

            L = Field[nuIdx][modeIdx].L
            M = Field[nuIdx][modeIdx].M
            O = Field[nuIdx][modeIdx].O
            MG = 2*M + L - 1

            title = f'{1e9*Field[nuIdx][modeIdx].wavelength:0.2f} nm; L=' + str(L) +\
                    '; M='+ str(M) +'; O=' + str(O) + '; MG=' + str(MG)


            xlabel = r'$x\,[\mu m]$'
            ylabel = r'$y\,[\mu m]$'
            xticklabels = [f'{Field[nuIdx][modeIdx].minX*1e6:.2f}', 0, f'{Field[nuIdx][modeIdx].maxX*1e6:.2f}']
            yticklabels = [f'{Field[nuIdx][modeIdx].maxY*1e6:.2f}', 0, f'{Field[nuIdx][modeIdx].minY*1e6:.2f}']
            
            if plotType == 'E':
               pltAx.imshow(ComplexArrayToRgb(Field[nuIdx][modeIdx].E))
            elif plotType == 'I':
               if colormap == 'complex':
                  pltAx.imshow(ComplexArrayToRgb(np.abs(Field[nuIdx][modeIdx].E)**2))
               else:
                  pltAx.imshow(np.abs(Field[nuIdx][modeIdx].E)**2, cmap = colormap)

            self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                                     xticklabels, yticklabels, 
                                     Field[nuIdx][modeIdx].dimX,Field[nuIdx][modeIdx].dimY)       
         else:
            raise Exception('''plot() with Field input being a list requires
                               `nuIdx` and `modeIdx` keyword arguments''')

                  

   def interactive_plot(self, Field, **kwargs):
      
      space = kwargs.pop('space', 'normal')
      plotType = kwargs.pop('plotType', 'E')
      colormap = kwargs.pop('colormap', 'viridis')
      line_style = kwargs.pop('line_style', '-') 
      color = kwargs.pop('color', [1,0,0])

      if isinstance(Field,list):

         def update_modeIdx_range(*args):
            modeIdx_widget.max = len(Field[nuIdx_widget.value])-1

         nuIdx_widget = widgets.IntSlider(min=0, max=len(Field)-1, step=1, value=0, 
                                          continuous_update = False)
         modeIdx_widget = widgets.IntSlider(min=0, max=len(Field[nuIdx_widget.value])-1,
                                            step=1, value=0,
                                            continuous_update = False)
         nuIdx_widget.observe(update_modeIdx_range,'value')                                   

         plt = interactive(self.plot, Field = fixed(Field), space = fixed(space), 
                           pos = fixed((0,0)),
                           plotType = fixed(plotType),
                           colormap = fixed(colormap),
                           line_style = fixed(line_style), color = fixed(color),
                           nuIdx = nuIdx_widget,
                           modeIdx = modeIdx_widget
                           )  

      elif not isinstance(Field,list):                                               
         if Field.option in ['xyknu']:
            plt = interactive(self.plot, Field = fixed(Field), space = fixed(space), 
                              pos = fixed((0,0)),
                              plotType = fixed(plotType),
                              colormap = fixed(colormap),
                              line_style = fixed(line_style), color = fixed(color),
                              nuIdx = widgets.IntSlider(min=0, max=Field.dimNU-1,
                                                      step=1, value=0,
                                                      continuous_update = False))  
      return plt               

   def plot_field_slice(self, Field, space = 'normal', pos = (0,0), **kwargs):   

      if self.figureLayout == 'Single':
         pltAx = self.axes
      elif self.figureLayout == '1D':
         pltAx = self.axes[pos[0] + pos[1]]
      elif self.figureLayout == '2D': 
         pltAx = self.axes[pos[0], pos[1]]

      title = kwargs.pop('title','')   

      if space == 'normal':
         xlabel = r'$x\,[\mu m]$'
         ylabel = r'$y\,[\mu m]$'
         xticklabels = [f'{Field.minX*1e6:.2f}', 0, f'{Field.maxX*1e6:.2f}']
         yticklabels = [f'{Field.maxY*1e6:.2f}', 0, f'{Field.minY*1e6:.2f}']
      elif space == 'fourier':
         xlabel = r'$k_x\,[m^{-1}]$'
         ylabel = r'$k_y\,[m^{-1}]$'
         xticklabels = [f'{Field.kminX:.2E}', 0, f'{Field.kmaxX:.2E}']
         yticklabels = [f'{Field.kmaxY:.2E}', 0, f'{Field.kminY:.2E}']

      if Field.option == 'xyz':
         if 'zIdx' in kwargs:
            zIdx = kwargs.get('zIdx')
            title = f'z = {1e6*Field.samplingZ[zIdx]:0.2f} um'
            pltAx.imshow(ComplexArrayToRgb(Field.E[:,:,kwargs.get('zIdx')]))
         else:
            raise Exception('plot_field_slice() requires `zIdx` keyword argument')

      elif Field.option == 'xyzk':
         if 'zIdx' in kwargs:
            zIdx = kwargs.get('zIdx')
            title = f'z = {1e6*Field.samplingZ[zIdx]:0.2f} um'

            if space == 'normal':
               pltAx.imshow(ComplexArrayToRgb(Field.E[:,:,kwargs.get('zIdx')]))
            elif space == 'fourier':
               pltAx.imshow(ComplexArrayToRgb(Field.Sk[:,:,kwargs.get('zIdx')]))
         else:
            raise Exception('plot_field_slice() requires `zIdx` keyword argument')    
      
      elif Field.option == 'xyzknu':
         required_kwargs = ['nuIdx', 'zIdx']
         if all (key in kwargs for key in required_kwargs):
            zIdx = kwargs.get('zIdx')
            nuIdx = kwargs.get('nuIdx')
            title = f'{1e9/Field.samplingNU[nuIdx]:0.2f} nm, {1e6*Field.samplingZ[zIdx]:0.2f} um'

            if space == 'normal':
               pltAx.imshow(np.abs(Field.E[:,:,zIdx,nuIdx])**2, cmap = 'viridis')
               # pltAx.imshow(ComplexArrayToRgb(Field.E[:,:,zIdx,nuIdx])) #NOTE:This was just a temporary fix for Mickael's philantropic grant
            elif space == 'fourier':
               pltAx.imshow(ComplexArrayToRgb(Field.Sk[:,:,zIdx,nuIdx]))           
         else:
            raise Exception('plot_field_slice() requires "zIdx" and "nuIdx" keyword arguments')
            
      self.setTitleLabelsTicks(pltAx, title, xlabel, ylabel, 
                               xticklabels, yticklabels, Field.dimX, Field.dimY)                              

   
   def interactive_plot_field_slice(self, *args, **kwargs):
      if args[0].option in ['xyz','xyzk']:
         plt = interactive(self.plot_field_slice, Field = fixed(args[0]),
                        space = fixed(kwargs['space']),
                        pos = fixed((0,0)),
                        zIdx = widgets.IntSlider(min=0, max=args[0].dimZ-1,
                                                  step=1, value=0,
                                                  continuous_update = False)) 
      elif args[0].option in ['xyzknu']:
         plt = interactive(self.plot_field_slice, Field = fixed(args[0]),
                           space = fixed(kwargs['space']),
                           pos = fixed((0,0)),
                           zIdx = widgets.IntSlider(min=0, max=args[0].dimZ-1,
                                                   step=1, value=0,
                                                   continuous_update = False),
                           nuIdx = widgets.IntSlider(min=0, max=args[0].dimNU-1,
                                                   step=1, value=0,
                                                   continuous_update = False))  
      return plt

                                                                                                 

   def setTitleLabelsTicks(self, ax, title, xlabel, ylabel, xticklabels, yticklabels, dimX, dimY):
      ax.set_title(title)
      ax.set(xlabel = xlabel, ylabel = ylabel)
      ax.set_xticks(np.linspace(0, dimX-1, 3))
      ax.set_xticklabels(xticklabels, color='black')
      ax.set_yticks(np.linspace(0, dimY-1, 3))
      ax.set_yticklabels(yticklabels, color='black')

   
   def save_figure_as_png(self, filename, dpi = 600):
      plt.gcf()
      plt.savefig(os.path.join(os.getcwd(), filename), dpi = dpi)
         










# ================== Qt plotting related ============================================
from PyQt5.QtCore import Qt # pylint: disable-msg=E0611
from PyQt5.QtWidgets import (QApplication, QHBoxLayout, QLabel, QSizePolicy, QSlider, QSpacerItem, QVBoxLayout, QWidget, QGridLayout) # pylint: disable-msg=E0611
import pyqtgraph as pg

import threading

from matplotlib import cm


def start_qt_app():
   app = QApplication.instance()  
   if app is None: #if there is no Qt app instance, launch it!
      app = QApplication([])

   return app   
      

class Slider(QWidget):
    def __init__(self, minimum, maximum, parent=None):
        super().__init__(parent=parent)

        self.verticalLayout = QVBoxLayout(self)
        self.label = QLabel(self)
        self.verticalLayout.addWidget(self.label)

        self.horizontalLayout = QHBoxLayout()
        self.slider = QSlider(self)
        self.slider.setOrientation(Qt.Vertical)
        self.resize(self.sizeHint())
        self.horizontalLayout.addWidget(self.slider)
        

        self.verticalLayout.addLayout(self.horizontalLayout)
        self.minimum = minimum
        self.maximum = maximum
        self.slider.valueChanged.connect(self.setLabelValue)
        self.value = None
        
        self.setLabelValue(self.slider.value())

    def setLabelValue(self, value):
        self.value = round(self.minimum + (float(value) / (self.slider.maximum() - self.slider.minimum())) * (self.maximum - self.minimum))
        self.label.setText(f"{self.value:02d}")


class QtSingleFieldStack(QWidget):
    def __init__(self, field_stack, width = 550, height = 500, parent = None):

        super().__init__(parent = parent)

        self.field_stack = field_stack
        print(np.max(np.abs(field_stack)))
        self.number_of_slices_in_stack = self.field_stack.shape[0]

        self.width = width
        self.height = height

        self.initUI() 
        self.show()

        self.update_plot()

    def initUI(self):    
        # Bunch of config settings
        pg.setConfigOptions(antialias=False)
        pg.setConfigOptions(imageAxisOrder='row-major')
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')


        self.layout = QGridLayout()
        self.resize(self.width,self.height)
        self.setWindowTitle('Field stack')
        self.setLayout(self.layout)  

        # Add interactive slider
        self.widget1 = Slider(0, self.number_of_slices_in_stack - 1)
        self.layout.addWidget(self.widget1, 0, 0, 10, 1) 
        self.widget1.slider.valueChanged.connect(self.update_plot)
        
        # Add image
        self.fig1 = CustomImageView()
        self.layout.addWidget(self.fig1, 0, 1, 10, 10)
              
    def update_plot(self):   
        slice_number = int(self.widget1.value)
        self.fig1.setImage(ComplexArrayToRgb(self.field_stack[slice_number])) 

        
class QtMPLCFieldStack(QWidget):
    def __init__(self, field_stack, mask_stack, field_after_last_mask_stack, field_infront_first_mask_stack,\
                 SOURCE_field_stack, SOURCE_intensity_TOTAL, TARGET_field_stack, TARGET_field_stack_achieved, TARGET_intensity_TOTAL, width = 1500, height = 500, parent = None):

        super().__init__(parent=parent)

        self.field_stack = field_stack
        self.mask_stack = mask_stack

        self.field_after_last_mask_stack = field_after_last_mask_stack
        self.field_infront_first_mask_stack = field_infront_first_mask_stack

        self.SOURCE_field_stack = SOURCE_field_stack
        self.SOURCE_intensity_TOTAL = SOURCE_intensity_TOTAL

        self.TARGET_field_stack = TARGET_field_stack
        self.TARGET_field_stack_achieved = TARGET_field_stack_achieved
        self.TARGET_intensity_TOTAL = TARGET_intensity_TOTAL

        self.number_of_planes = self.field_stack.shape[1]
        self.number_of_slices_in_stack = self.field_stack.shape[2] # [0] is 'direction'; [1] is number of MPLC planes

        self.width = width
        self.height = height

        self.initUI()
        self.show()

        self.update_plot()
        

    def initUI(self):    
        # Bunch of config settings
        pg.setConfigOptions(antialias=False)
        pg.setConfigOptions(imageAxisOrder='row-major')
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')


        self.layout = QGridLayout()
        self.resize(self.width,self.height)
        self.setWindowTitle('Field stack')
        self.setLayout(self.layout)  

        # Add interactive slider
        self.widget1 = Slider(0, self.number_of_slices_in_stack - 1)
        self.layout.addWidget(self.widget1, 0, 0, 10, 1) 
        self.widget1.slider.valueChanged.connect(self.update_plot)
        
        # Add images
        self.figures_forward = []
        self.figures_backward = []
        self.masks = []

        for plane_idx in range(self.number_of_planes + 4): # +4 because on top of fields at each MPLC plane I plot :  SOURCE fields; fields in front/after the last mask; TARGET FIELDS 
           self.figures_forward.append(CustomImageView())
           self.figures_backward.append(CustomImageView())
           self.masks.append(CustomImageView())
           self.layout.addWidget(self.figures_forward[-1], 0, 1 + plane_idx*5, 3, 5)
           self.layout.addWidget(self.figures_backward[-1], 3, 1 + plane_idx*5, 3, 5)
           self.layout.addWidget(self.masks[-1], 6, 1 + plane_idx*5, 3, 5)
            

    def update_plot(self):
       for plane_idx in range(self.number_of_planes + 4): # +4 because on top of fields at each MPLC plane I plot : SOURCE fields; fields in front/after the last mask; TARGET FIELDS
            t = threading.Thread(target = self.update_plot_worker, args=[plane_idx])
            t.start()   

    def update_plot_worker(self, plane_idx):
        slice_number = int(self.widget1.value)

        if plane_idx == 0: # plotting SOURCE fields
            self.figures_forward[plane_idx].setImage(ComplexArrayToRgb(self.SOURCE_field_stack[slice_number]))
            self.masks[plane_idx].setImage(ComplexArrayToRgb(self.SOURCE_intensity_TOTAL)) # Not plotting a mask here but let's use the space in this row

        elif plane_idx == 1: # plotting field in front of the first mask (to compare how close the backpropagated field is to the launched FIELD_IN)
            self.figures_forward[plane_idx].setImage(ComplexArrayToRgb(self.field_stack[0][0][slice_number]))
            self.figures_backward[plane_idx].setImage(ComplexArrayToRgb(self.field_infront_first_mask_stack[slice_number]))

        elif plane_idx == self.number_of_planes + 2: # plotting field after the last mask (to compare how close the propagated field is to the FIELD_OUT)
            self.figures_forward[plane_idx].setImage(ComplexArrayToRgb(self.field_after_last_mask_stack[slice_number]))
            self.figures_backward[plane_idx].setImage(ComplexArrayToRgb(self.field_stack[1][-1][slice_number]))

        elif plane_idx == self.number_of_planes + 3: # plotting TARGET FIELDS (both requested and achieved)   
            self.figures_forward[plane_idx].setImage(ComplexArrayToRgb(self.TARGET_field_stack_achieved[slice_number]))
            self.figures_backward[plane_idx].setImage(ComplexArrayToRgb(self.TARGET_field_stack[slice_number]))
            self.masks[plane_idx].setImage(ComplexArrayToRgb(self.TARGET_intensity_TOTAL)) # Not plotting a mask here but let's use the space in this row

        else: # plotting all the other fields
            self.figures_forward[plane_idx].setImage(ComplexArrayToRgb(self.field_stack[0][plane_idx - 2][slice_number]))
            self.figures_backward[plane_idx].setImage(ComplexArrayToRgb(self.field_stack[1][plane_idx - 2][slice_number]))
            self.masks[plane_idx].setImage(map_field_to_mask(self.mask_stack[plane_idx - 2]))


                  
# Utility functions

def rgb2gray(rgb):
   return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])

def generatePgColormap(cm_name):
   pltMap = cm.get_cmap(cm_name)
   colors = pltMap.colors
   colors = [c + [1.] for c in colors]
   positions = np.linspace(0, 1, len(colors))
   pgMap = pg.ColorMap(positions, colors)
   return pgMap

class CustomImageView(pg.ImageView):

   def __init__(self, *args, **kwargs):
      super().__init__(*args, **kwargs)

      # I rarely use the functionality of controls below, so hiding those
      self.ui.histogram.hide() 
      self.ui.roiBtn.hide()
      self.ui.menuBtn.hide()
      self.ui.roiPlot.hide()

   def setImage(self, image, levels = (0, 1)): 
      # setting the 'levels' ensures that the histogram does not go haywire which caused me a lot of grief when playing with field stacks
      super().setImage(image, levels = levels)   


#* ================================= PLOTTING complex fields =========================================================================

from cv2 import (cvtColor, COLOR_HSV2RGB) # pylint: disable-msg=E0611
import numexpr as ne
from numba import jit
import PIL.Image
import io


def ComplexArrayToRgb(Efield, normalise = True, conversion = ('standard', 'custom')[0], theme = ('dark', 'light')[0]):
   # ne.set_vml_accuracy_mode('low')
   absEfield = ne.evaluate('real(abs(Efield))', {'Efield': Efield})
   
   HSV = np.zeros((Efield.shape[0], Efield.shape[1], 3), dtype = np.float32)
   HSV[:, :, 0] = ne.evaluate('360*(arctan2(imag(Efield),real(Efield))/(2*pi) % 1)', {'Efield': Efield,'pi':np.pi})
   
   if conversion == 'standard':
      if theme == 'dark':
         HSV[:, :, 1] = 1
         if normalise:
            HSV[:, :, 2] = absEfield/absEfield.max()
         else:   
            HSV[:, :, 2] = absEfield
            
         RGB = cvtColor(HSV, COLOR_HSV2RGB)
      elif theme == 'light':
         HSV[:, :, 2] = 1
         if normalise:
            HSV[:, :, 1] = absEfield/absEfield.max()
         else:   
            HSV[:, :, 1] = absEfield
            
         RGB = cvtColor(HSV, COLOR_HSV2RGB)   
         
   elif conversion == 'custom':
      # Inspired by: https://www.mathworks.com/matlabcentral/fileexchange/69930-imagecf-complex-field-visualization-amplitude-phase
      
      RGB = np.zeros((Efield.shape[0], Efield.shape[1], 3), dtype = np.float32)
      if normalise:
         R = (absEfield/absEfield.max())
      else:
         R = absEfield   
      c = np.cos(np.angle(Efield)).astype(np.float32)
      s = np.sin(np.angle(Efield)).astype(np.float32)
         
      if theme == 'dark':
         
         RGB[:, :, 0] = np.abs((1/2 + np.sqrt(6)/4 * ( 2*c/np.sqrt(6)    ))* R) # values can go marginally below zero and then it clips. np.abs gets rid of the issue
         RGB[:, :, 1] = np.abs((1/2 + np.sqrt(6)/4 * (- c/np.sqrt(6) + s/np.sqrt(2) ))* R)
         RGB[:, :, 2] = np.abs((1/2 + np.sqrt(6)/4 * (- c/np.sqrt(6) - s/np.sqrt(2) ))* R)
         
      elif theme == 'light':
         # NOTE R is the normal mapping but looks too sharp on white background, R**2 looks kind of perceptionally correct on white compared to HSV conversion but only use it in schematics as the mapping is wacko:)
         
         RGB[:, :, 0] = np.abs(1 - (1/2 + np.sqrt(6)/4 * (- 2*c/np.sqrt(6)    ))* R**2)  
         RGB[:, :, 1] = np.abs(1 - (1/2 + np.sqrt(6)/4 * (+ c/np.sqrt(6) - s/np.sqrt(2) ))* R**2)
         RGB[:, :, 2] = np.abs(1 - (1/2 + np.sqrt(6)/4 * (+ c/np.sqrt(6) + s/np.sqrt(2) ))* R**2)    
   
   return RGB


def convert_data_to_ipython_image_widget_format(data):
   """
   Helper function to compress image data via PIL/Pillow.
   """
   image_data = (ComplexArrayToRgb(data)*255).astype(np.uint8)
   buff = io.BytesIO()
   img = PIL.Image.fromarray(image_data)    
   img.save(buff, format='png')
   
   return buff.getvalue()


def map_field_to_mask(field): 

   return (np.angle(field) + np.pi)/(2*np.pi)


def add_complex_colorbar(ax, offset = 0.03, phase_count = 512, amplitude_count = 64):
   
   def make_complex_colorgrid(phase_count, amplitude_count):
      _amplitude = np.linspace(0, 1, amplitude_count)
      _phase = np.linspace(-np.pi, np.pi, phase_count)
      amplitude, phase = np.meshgrid(_amplitude,_phase)
      color_grid = amplitude*np.exp(1j*phase)
      
      return color_grid

   ax.imshow(ComplexArrayToRgb(make_complex_colorgrid(phase_count, amplitude_count)))

   ax.set_xticks([0, amplitude_count])
   ax.set_xticklabels([0,1])

   ax.yaxis.tick_right()
   ax.set_yticks([0, phase_count//2, phase_count])
   ax.set_yticklabels([r'$+\pi$',0,r'$-\pi$'])

   positions = ax.get_position()
   points = positions.get_points()
   points[0,0] -= offset
   points[1,0] -= offset
   positions.set_points(points)
   ax.set_position(positions)
#* ------------------------------------------------------------------------------------------------------------------------------------

if __name__ == '__main__':
   print('Module can run!')

   # import mkl
   # cpu_clock = mkl.get_cpu_frequency()
   # threads = mkl.get_max_threads()
   # mkl.set_num_threads(8)
   # threads = mkl.get_max_threads()
   # mkl_version = mkl.get_version_string()
   # mem_stat = mkl.mem_stat()

   # print(f'CPU clock: {cpu_clock}; number of threads:{threads}; Memory:{mem_stat}')
   # print(mkl_version)
   # np.__config__.show()






   # w0 = 3
   # N = 2000
   # X,Y = np.meshgrid(np.linspace(-5,5,N, dtype = np.float32),np.linspace(-5,5,N,dtype = np.float32))
   # rho, theta = cart2pol(X,Y)
   # field = np.exp(-(rho**2)/w0**2)*np.exp(-1j*theta)


   # tic1 = time.time()
   # hsv_image = ComplexArrayToRgb(field)      
   # toc1 = time.time()

   # print(f"Time to plot:{toc1-tic1}")

   # fig, ax = plt.subplots(nrows = 1, ncols = 1)
   # fig.set_size_inches(9, 4)
   # fig.subplots_adjust(wspace = 0.4, hspace = 0)

   # ax.imshow(hsv_image)

   # plt.show()
